﻿
// https://github.com/mapbox/node-sqlite3/wiki/API

//const SimpleNodeLogger = require('simple-node-logger')
const sqlite3 = require('sqlite3')//.verbose();

//const log = SimpleNodeLogger.createSimpleFileLogger( {
  //  logFilePath: 'logs/sq.log',
    //timestampFormat:'YYYY-MM-DD HH:mm:ss.SSS'
//} );



const execute = function(db, sql, params) {

    let stmt = db.prepare(sql);
    let rc =  new Promise((r, j) => {
            stmt.run(params, function(err) {                
                if(!err) r(this); // null if success
                else j(err)
            })
        })
        
    .then( v => {
  //          log.info(v)
            stmt.finalize()
            return v

        }).catch(x => {
    //        log.warn(x)
            stmt.finalize()
            return null // see log
        })


    return rc
}

// ...............................................................................

const first = function(db, sql, params) {
    let stmt = db.prepare(sql);

    let rc = new Promise((r, j) => {
        stmt.get(params, function(err, row) {
                if(!err) r(row);
                else j(err)
            })
    })
        
    .then( v => {
    //        log.info(v)
            stmt.finalize()
            return v    // row or undefined
        }).catch(x => {
  //          log.warn(x)
            stmt.finalize()
            return null // see log
        })

    return rc
}

// ...............................................................................

const all = function(db, sql, params) {
    let stmt = db.prepare(sql);

    let rc = new Promise((r, j) => {
        stmt.all(params, function(err, row) {
                if(!err) r(row);
                else j(err)
            })
    })
        
    .then( v => {
      //      log.info(v)
            stmt.finalize()
            return v    // row or undefined
        }).catch(x => {
        //    log.warn(x)
            stmt.finalize()
            return null // see log
        })

    return rc
}

module.exports = {execute, first, all}

