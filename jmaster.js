﻿const http = require("http");
const express = require( "express");
const bodyParser = require('body-parser')
const WebSocket = require( "ws");
const uuid = require('uuid');
const j = require('jwt-decode')
const e = require('jwt-encode')
const ut = require('unix-timestamp')

const sqlite3 = require('sqlite3')
const sq = require('./sq')
const db = new sqlite3.Database('./db/jmaster.sq3');

const app = express()
const port = 8100

const server = http.createServer(app);
const webSocketServer = new WebSocket.Server({ server });

const clientSet = new Map()

app.use(express.json())
//app.use(bodyParser.json())
app.use(express.urlencoded({ extended: true }))

app.use(express.static('public', {
    dotfiles: 'ignore',
    etag: false,
    extensions: ['htm', 'html'],
    index: false,
    maxAge: '1d',
    redirect: false,
    setHeaders: function (res, path, stat) {
    res.set('x-timestamp', Date.now())
  }
}))

// .......................................................................................................


app.get('/', (req, res, next) => {
    res.send('jmaster')
    next()
})

// .......................................................................................................


app.get('/agentHub', (req, res, next) => {
    console.log("GET " + req.path)
    res.send('jmaster')
    next()
})

// .......................................................................................................

app.post('/command', (req, res) => {
    console.log(req.body)
    res.send(`{
            "res":"ok"
        }`
    )

    let c = req.body.code
    webSocketServer.clients.forEach(client => {
        let tr = `{"type":${c}}`
        client.send(tr);
    })

})

// .......................................................................................................

async function dbqueryProject(id) {
    let sql = "select project from agents where agent_id=?"
    let res = await sq.first(db, sql, [id])
//    console.log(res)    
    return res
}

// .......................................................................................................

async function getDataForToken(body) {
    console.log(body.machinekey)
    const project = await dbqueryProject(body.machinekey)
    console.log(project)
    if(!project) {
        // agent is not registered
        return null        
    }

    const mn = project
    const tdata = {
      "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name": project,
      "target": 'hub',
      "nbf": ut.now(),
      "exp": ut.now(60*60)
    }
    jwt = e(tdata, "")
    const data = `{
        machinename: "${mn}",
        access_token: "${jwt}"
    }`
    return data
}

// .......................................................................................................

app.post('/token', async (req, res, next)  =>  
    {
        console.log("b " + JSON.stringify(req.body))
        let data = await getDataForToken(req.body)

        console.log(data)
        res.send(data)
    }
)

// .......................................................................................................

function neg() {
    let n = {
        "connectionId":uuid.v4(), //"H8xq0Q980Q7BNgtPFxPCTw",
        "availableTransports":
        [
            {
                "transport":"WebSockets",
                "transferFormats":["Text","Binary"]},{"transport":"ServerSentEvents","transferFormats":["Text"]},
            {
                "transport":"LongPolling","transferFormats":["Text","Binary"]
            }
        ]
    }
    return n
}

// .......................................................................................................

app.post('/agentHub/negotiate', (req, res) => {
    console.log(req.path)    
    res.send(JSON.stringify(neg()));
})

// .......................................................................................................

function heartbeat() {
    console.log('pong') 
    this.isAlive = true;  // connection
}

// .......................................................................................................

function processMessage(client, msg) {
    let m = JSON.parse(msg.slice(0, -1))
    if(m.protocol) {
        console.log("p " + m.protocol)
        client.send("{}")
    } else if(m.type) {

        let tr = `{"type":6}`
        let tr1 = `{"type":1,"invocationId":"1","target":"Version","arguments":["1.0.0.0"]}`    // ????
        let tr3 = `{"type":3,"invocationId":"1","result":"1.0.0.0"}`

        switch(m.type) {
                    case 1:
                        tr = tr3
//console.table(client)
                        break;
                    case 3: 
                        break;
                    case 6:
                        break;
                    default:
                        break;
        }
        console.log("type " + m.type)
        client.send(tr)
    }
}

// .....................................................................................................

webSocketServer.on('connection', (ws, request) => {
    const Id = request.headers["sec-websocket-key"];
    clientSet.set(Id, ws);
//console.table(request)
    ws.on('message', msg => {
        console.log("ws on message " + msg)
        webSocketServer.clients.forEach(client => {
            //client.send("OK")
            processMessage(client, msg)
        });
    });

    ws.on("error", e => {
        console.log(e)
        ws.send(e)
    });

    ws.on('pong', heartbeat)

    ws.on('close', function () {
        console.log("close")
        clientSet.delete(Id);
    });
        
    console.log("ws on connection")
   //   ws.send(`{"protocol":"json","version":"1"}`);
});

// .................................................................................................

const interval = setInterval(function () {
    webSocketServer.clients.forEach(connection =>  {
        if (connection.isAlive === false) {
            console.log("Connection died", connection.id);
            return connection.terminate();
        }

        // Request the client to respond with pong. Client does this automatically.
        connection.isAlive = false;
        connection.ping(() =>  {
            console.log('ping')
        });
    });
}, 10000);


// .................................................................................................

webSocketServer.on('close', function close() {
    clearInterval(interval);
});

server.on('upgrade', (request, socket, head) => {
    console.log("upgrade " + head);
})

// .......................................................................................................

server.listen(port, () => {
    console.log(`Listening on port ${port}`)
})
